function Abc(_x) {
    return "abc" + _x;
}
;
var clickFn = function () {
    console.info("hello man");
    var textEl = document.getElementById("text1");
    var text = textEl.value;
    var li = document.createElement('li');
    var del = document.createElement('input');
    del.type = 'button';
    del.value = 'удалить';
    li.innerText = text + Abc(text.length);
    li.appendChild(del);
    document.getElementById("list").appendChild(li);
    del.addEventListener("click", function () {
        document.getElementById("list").removeChild(li);
    });
};
document.getElementById("Button1").addEventListener("click", clickFn);
