"use strict";

//******************************************************************************
//* DEPENDENCIES
//******************************************************************************

var gulp        = require("gulp"),
    tsc         = require("gulp-typescript"),
    concat      = require("gulp-concat"),
    uglify      = require("gulp-uglify"),
    webpack     = require("webpack"),
    runSequence = require("run-sequence");

//******************************************************************************
//* BUILD
//******************************************************************************
var tstProject = tsc.createProject("tsconfig.json", { typescript: require("typescript") });

gulp.task("build", function() {
    return gulp.src([
        "src/**/*.ts"
    ])
    .pipe(tstProject())
    .on("error", function (err) {
        process.exit(1);
    })
    .pipe(gulp.dest("dist/"));
});


//gulp.task("uglify",["build"], function() {
//    return gulp.src([
//        "dist/**/*.js"
//    ])
//    .pipe(uglify({ output: { ascii_only:true } }))
//    .pipe(concat("player.min.js"))
//    .pipe(gulp.dest("dist2/"));
//});


/*
gulp.task("webpack",["build"], function(callback) {
    // run webpack
    webpack({
        entry: __dirname + '/dist/app',
        output: {
            path: __dirname + "/bundle",
            filename: "app.js"
        }
        // configuration
    }, function(err, stats) {
        if(err)
            throw new gutil.PluginError("webpack", err);
        callback();
    });
});*/
//******************************************************************************
//* DEFAULT
//******************************************************************************
gulp.task("default", function (cb) {
  runSequence("build", cb);
});