var compression = require('compression');
const args = require("node-args-parser")(process.argv);
const path = require('path');
var mustacheExpress = require('mustache-express');

var express = require('express');
var app = express();

// Register '.mustache' extension with The Mustache Express
app.engine('mustache', mustacheExpress());
app.use(compression());

// Routes
app.use('/lib', express.static(path.resolve(path.join(__dirname, "../dist"))));
app.use('/libs', express.static(path.resolve(path.join(__dirname, "../libs"))));
app.use('/assets', express.static(path.resolve(path.join(__dirname, "../assets"))));
app.use('/css', express.static(path.resolve(path.join(__dirname, "../css"))));
app.get('/', function (req, res) {
	res.render(
		'app');
});

// Setting view
app.set('view engine', 'mustache');
app.set("views", path.join(__dirname, "views"));

// go
app.listen(args.port);
console.info("starting listening on port "+args.port);