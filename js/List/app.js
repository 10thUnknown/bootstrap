console.info('go!');

var data = [1, 2, 3, 4];
var dump = function () {
    console.info('data = [' + data.join(',') + ']');
};
dump(data);

var add = function (_x) {
    data.push(_x);
};
var insert = function (_index, _x) {
    data.splice(_index,0,_x);

};
var remove = function (_x) {
    data.splice(data.indexOf(_x),1);



};
var removeAt = function (_index) {
    data.splice(_index,1);

};
var move = function (_indexA, _indexB) {
    var swap = data[_indexA];
    data[_indexA]=data[_indexB];
    data[_indexB]=swap;


};
var clear = function () {
    data.length = 0;



};


add(5);
dump();

insert(2,2);
dump();

remove(4);
dump();

removeAt(1);
dump();

move(1,3);
dump();

clear();
dump();

